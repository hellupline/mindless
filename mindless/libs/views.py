# -*- coding: utf-8 -*-

from functools import wraps
from flask import jsonify
from .session import session_scope


def json(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        return jsonify(f(*args, **kwargs))
    return decorated_function


def db(session):
    def decorator(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            with session_scope(session) as s:
                return f(s, *args, **kwargs)
        return decorated_function
    return decorator


def get_slice(args):
    try:
        limit = int(args.get('limit'))
    except (ValueError, TypeError):
        limit = 20
    try:
        offset = int(args.get('offset'))
    except (ValueError, TypeError):
        offset = 0
    return limit, offset


def filter_dict(data, fields):
    return {k: v for k, v in data.items() if k in fields}
