# -*- coding: utf-8 -*-

import unittest

from mindless import create_app
from mindless.config import Test
from mindless.extensions import db

app = create_app(Test)


class BaseTestCase(unittest.TestCase):
    maxDiff = None

    @classmethod
    def setUpClass(cls):
        app.config['CSRF_ENABLED'] = False

        with app.test_request_context():
            db.create_all()
            if hasattr(cls, 'fixture'):
                cls.fixture(db.session)

        cls.app = app.test_client()

    @classmethod
    def tearDownClass(cls):
        with app.test_request_context():
            db.session.remove()
            db.drop_all()
