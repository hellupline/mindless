# -*- coding: utf-8 -*-


class ModelMixin(object):
    def __iter__(self):
        if hasattr(self, '_map_fields'):
            map_fields = getattr(self, '_map_fields')
            for c in self.__table__.columns:
                if c.name in map_fields:
                    yield c.name, getattr(self, c.name)
        else:
            for c in self.__table__.columns:
                yield c.name, getattr(self, c.name)

    def __repr__(self):
        if hasattr(self, 'slug'):
            object_name = self.slug
        elif hasattr(self, 'name'):
            object_name = self.name
        elif hasattr(self, 'title'):
            object_name = self.title
        elif hasattr(self, '_class_name'):
            object_name = getattr(self, self._class_name)
        else:
            return '<{class_name}>'.format(class_name=self.__class__.__name__)
        return '<{class_name} {object_name}>'.format(
            class_name=self.__class__.__name__, object_name=object_name)

    def map(self):
        return dict(self)

    def update(self, values):
        for key, value in values.items():
            if hasattr(self, key):
                setattr(self, key, value)
