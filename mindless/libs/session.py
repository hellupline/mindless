# -*- coding: utf-8 -*-

from contextlib import contextmanager

from flask import abort
from sqlalchemy.orm.exc import NoResultFound, MultipleResultsFound


@contextmanager
def session_scope(session, close=True):
    """Provide a transactional scope around a series of operations."""
    try:
        yield session
        session.commit()
    except:
        session.rollback()
        raise
    finally:
        if close:
            session.close()


def get_or_fail(session, model, query):
    try:
        row = session.query(model).filter(query).one()
    except NoResultFound:
        abort(404)
    except MultipleResultsFound:
        abort(500)
    return row
