# -*- coding: utf-8 -*-

import unicodedata
import re

from bs4 import BeautifulSoup, Comment
import htmlmin


VALID_TAGS = {
    'strong': (),
    'em': (),
    'p': (),
    'ol': (),
    'ul': (),
    'li': (),
    'br': (),
    'img': ('src',),
    'a': ('href', 'title'),
}


def first_or_str(values):
    try:
        return values[0]
    except IndexError:
        return ''


def slugify(value):
    value = unicodedata.normalize('NFKD', value)
    value = value.encode('ascii', 'ignore').decode('ascii')
    value = re.sub(r'[^\w\s-]', '', value)
    value = re.sub(r'[-\s]+', '-', value)
    return value.strip().lower()


def text2bool(value, default=True):
    if value == 'true':
        return True
    elif value == 'false':
        return False
    elif value == '1':
        return True
    elif value == '0':
        return False
    return default


def sanitize_html(value, valid_tags=VALID_TAGS):
    # http://stackoverflow.com/questions/699468/python-html-sanitizer-scrubber-filter/812785#812785
    if not value:
        return ''
    soup = BeautifulSoup(value)

    for comment in soup.findAll(text=lambda text: isinstance(text, Comment)):
        comment.extract()

    # Some markup can be crafted to slip through BeautifulSoup's parser, so
    # we run this repeatedly until it generates the same output twice.
    newoutput = soup.renderContents()
    oldoutput = None
    while oldoutput != newoutput:
        oldoutput = newoutput
        soup = BeautifulSoup(newoutput)
        for tag in soup.findAll(True):
            if tag.name not in valid_tags:
                tag.hidden = True
            else:
                tag.attrs = {attr: value for attr, value in tag.attrs.items()
                             if attr in valid_tags[tag.name]}
        newoutput = soup.renderContents()
    return htmlmin.minify(newoutput.decode('utf-8'))


def quote_tags(tags):
    def quote(tag):
        if ' ' in tag:
            return '"{}"'.format(tag)
        else:
            return '{}'.format(tag)
    return map(quote, tags)


def split_strip(input, delimiter=','):  # from django tagging
    """
    Splits ``input`` on ``delimiter``, stripping each resulting string
    and returning a list of non-empty strings.
    """
    if not input:
        return []

    words = [w.strip() for w in input.split(delimiter)]
    return [w for w in words if w]


def parse_tag_input(input):  # from django tagging
    """
    Parses tag input, with multiple word input being activated and
    delineated by commas and double quotes. Quotes take precedence, so
    they may contain commas.

    Returns a sorted list of unique tag names.
    """
    if not input:
        return []

    # Special case - if there are no commas or double quotes in the
    # input, we don't *do* a recall... I mean, we know we only need to
    # split on spaces.
    if ',' not in input and '"' not in input:
        words = list(set(split_strip(input, ' ')))
        words.sort()
        return words

    words = []
    buffer = []
    # Defer splitting of non-quoted sections until we know if there are
    # any unquoted commas.
    to_be_split = []
    saw_loose_comma = False
    open_quote = False
    i = iter(input)
    try:
        while 1:
            c = next(i)
            if c == '"':
                if buffer:
                    to_be_split.append(''.join(buffer))
                    buffer = []
                # Find the matching quote
                open_quote = True
                c = next(i)
                while c != '"':
                    buffer.append(c)
                    c = next(i)
                if buffer:
                    word = ''.join(buffer).strip()
                    if word:
                        words.append(word)
                    buffer = []
                open_quote = False
            else:
                if not saw_loose_comma and c == ',':
                    saw_loose_comma = True
                buffer.append(c)
    except StopIteration:
        # If we were parsing an open quote which was never closed treat
        # the buffer as unquoted.
        if buffer:
            if open_quote and ',' in buffer:
                saw_loose_comma = True
            to_be_split.append(''.join(buffer))
    if to_be_split:
        if saw_loose_comma:
            delimiter = ','
        else:
            delimiter = ' '
        for chunk in to_be_split:
            words.extend(split_strip(chunk, delimiter))
    words = list(set(words))
    words.sort()
    return words
