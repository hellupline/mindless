# -*- coding: utf-8 -*-

import os

from .libs.utils import make_dir


class Default(object):
    PROJECT_ROOT = os.path.abspath(
        os.path.join(os.path.dirname(__file__), '..'))
    PROJECT = 'mindless'

    DEBUG = TESTING = False

    ADMINS = []
    SECRET_KEY = 'not-secret'

    LOG_FOLDER = os.path.join(PROJECT_ROOT, 'logs')

    MAX_CONTENT_LENGTH = 16*1024*1024
    UPLOAD_FOLDER = os.path.join(PROJECT_ROOT, 'uploads')

    # Flask-Sqlalchemy: http://packages.python.org/Flask-SQLAlchemy/config.html
    SQLALCHEMY_DATABASE_URI = 'sqlite:///{}/db.sqlite'.format(PROJECT_ROOT)
    SQLALCHEMY_ECHO = False

    # Flask-cache: http://pythonhosted.org/Flask-Cache/
    CACHE_TYPE = 'simple'
    CACHE_DEFAULT_TIMEOUT = 60

    # Flask-mail: http://pythonhosted.org/flask-mail/
    # https://bitbucket.org/danjac/flask-mail/issue/3/problem-with-gmails-smtp-server
    MAIL_DEBUG = DEBUG
    MAIL_SERVER = 'smtp.gmail.com'
    MAIL_PORT = 587
    MAIL_USE_TLS = True
    MAIL_USE_SSL = False
    MAIL_USERNAME = 'yourmail@gmail.com'
    MAIL_PASSWORD = 'yourpass'
    MAIL_DEFAULT_SENDER = MAIL_USERNAME


class Test(Default):
    SQLALCHEMY_DATABASE_URI = 'sqlite:///{}/test.sqlite'.format(
        Default.PROJECT_ROOT)
    DEBUG = TESTING = True


class Server(Default):
    SQLALCHEMY_DATABASE_URI = (
        ('{server_type}://{username}:{password}@{ip}/').format(
            server_type='postgres', ip='localhost',
            username='hellupline', password='123')
    )


make_dir(Default.LOG_FOLDER)
make_dir(Default.UPLOAD_FOLDER)
