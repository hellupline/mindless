(function() {
    'use strict';

    var api_url_list = '/brain/data/list/api/';
    var api_url_detail = '/brain/data/detail/api/';
    var api_url_add = '/brain/data/add/api/';
    var api_url_edit = '/brain/data/edit/api/';
    var api_url_delete = '/brain/data/delete/api/';

    var view_url_list = '/brain/data/list/';
    var view_url_detail = '/brain/data/detail/';

    function get_tags_from_data(data, tags) {
        var length = data['_tags'].length;
        for (var i=0; i < length; i++) {
            var tag = data['_tags'][i];
            var index = tags.indexOf(tag);
            if (index < 0) {
                tags.push(tag);
            }
        }
    };

    app.controller('DataAddController', function($scope, $http, $location) {
        $scope.breadcrumb_path = [
            {name: 'Brain', url:'/#!/brain/'},
            {name: 'Data', url:'/#!/brain/data/'},
            {name: 'Add', url:'/#!/brain/data/add/'}
        ];

        $scope.addData = function() {
            $http.post(api_url_add, $scope.new_data)
                .success(function(data) {
                    $location.path(view_url_detail).search({slug: data['object']['slug']});
                });
        };
    });

    app.controller('DataDetailController', function($scope, $http, $location) {
        var slug_url = $location.search().slug;

        $scope.breadcrumb_path = [
            {name: 'Brain', url:'/#!/brain/'},
            {name: 'Data', url:'/#!/brain/data/'},
            {name: 'Detail', url:'/#!/brain/data/detail/'}
        ];

        $scope.slug = slug_url ? slug_url : '';
        $scope.data = {};
        $scope.tag_list = [];
        $scope.orig = {};
        $scope.edit = false;

        function set_data_from_response(response) {
            $scope.data = response['object'];
            $scope.orig = angular.copy($scope.data);
            $scope.tag_list = [];
            if ($scope.data) {
                get_tags_from_data($scope.data, $scope.tag_list);
            }
        }

        $scope.loadData = function() {
            $http.get(api_url_detail, {params: {slug: $scope.slug}})
                .success(set_data_from_response)
                .error(function() { $location.path(view_url_list); });
            $scope.edit = false;
        };

        $scope.saveData = function() {
            $http.post(api_url_edit, $scope.data, {params: {slug: $scope.slug}})
                .success(set_data_from_response);
            $scope.edit = false;
        };

        $scope.resetData = function() {
            $scope.data = angular.copy($scope.orig);
            $scope.edit = false;
        };

        $scope.deleteData = function() {
            $http.post(api_url_delete, null, {params: {slug: $scope.slug}});
            $scope.edit = false;
            $location.path(view_url_list).search({}).replace();
        };
    });

    app.controller('DataListController', function($scope, $http, $location) {
        var filter_url = $location.search().filter;
        var next_filter = {};

        $scope.breadcrumb_path = [
            {name: 'Brain', url:'/#!/brain/'},
            {name: 'Data', url:'/#!/brain/data/'},
            {name: 'List', url:'/#!/brain/data/list/'}
        ];

        $scope.filter = filter_url ? filter_url.split(' ') : [];
        $scope.data_list = [];
        $scope.tag_list = [];

        function set_data_list_from_response(response) {
            $scope.data_list = $scope.data_list.concat(response['object_list']);
            $scope.tag_list = [];
            var length = $scope.data_list.length;
            for (var i=0; i < length; i++) {
                get_tags_from_data($scope.data_list[i], $scope.tag_list);
            }
            next_filter = response['next_page'];
        }

        $scope.loadData = function() {
            var filter = {tags: $scope.filter.join(' ')};
            angular.extend(filter, next_filter);
            $http.get(api_url_list, {params: filter})
                .success(set_data_list_from_response);
            /*
            if ($scope.filter == '') {
                $location.search({}).replace();
            } else {
                $location.search({filter: $scope.filter.join(' ')}).replace();
            }
            */
        };

        $scope.toggleFilter = function(tag) {
            if (tag != '') {
                var i = $scope.filter.indexOf(tag);
                if (~i) {
                    $scope.filter.splice(i, 1);
                } else {
                    $scope.filter.push(tag);
                }

                next_filter = {};
                $scope.data_list = [];
                $scope.tag_list = [];

                $scope.loadData();
            }
        };
    });
}());
