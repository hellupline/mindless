(function() {
    'use strict';

    var api_url_website_list = '/feed/website/list/api/';
    var api_url_post_list = '/feed/post/list/api/';
    var api_url_post_edit = '/feed/post/edit/api/';

    app.controller('PostListController', function($scope, $http, $location, $anchorScroll) {
        $scope.breadcrumb_path = [
            {name: 'Feed', url:'/#!/feed/'},
            {name: 'Post', url:'/#!/feed/post/'},
            {name: 'List', url:'/#!/feed/post/list/'}
        ];

        $scope.global_filter = {};
        $scope.scope_filter = {};
        $scope.next_filter = {};
        $scope.menu = [];
        $scope.posts = [];
        $scope.index = -1;

        function build_category(website_list) {
            var categories = {}, count = website_list.length;
            for (var i=0; i < count; i++) {
                var website = website_list[i];
                categories[website['category']] = categories[website['category']] || [];
                categories[website['category']].push(website);
            }
            return categories;
        }

        function set_menu(data) {
            var categories = build_category(data['object_list']);
            for (var cat_key in categories) {
                if (categories.hasOwnProperty(cat_key)) {
                    var websites = [], ws_count = categories[cat_key].length;
                    for (var i=0; i < ws_count; i++) {
                        var website = categories[cat_key][i];
                        websites.push({
                            title: website['title'],
                            filter: {website: website['id']}
                        });
                    }
                    $scope.menu.push({
                        title: cat_key,
                        filter: {category: cat_key},
                        websites: websites
                    });
                }
            }
        }

        function set_posts(data) {
            $scope.posts = $scope.posts.concat(data['object_list']);
            var length = $scope.posts.length;
            if (length) {
                $scope.next_filter = {lt_id: $scope.posts[length-1]['id']};
            } else {
                $scope.next_filter = {lt_id: 0};
            }
        }

        $scope.loadWebsites = function() {
            $http.get(api_url_website_list).success(set_menu);
        };

        $scope.loadPosts = function() {
            var filter = angular.copy($scope.global_filter);
            angular.extend(filter, $scope.scope_filter);
            angular.extend(filter, $scope.next_filter);
            $http.get(api_url_post_list, {params: filter}).success(set_posts);
        };

        $scope.loadFilter = function() {
            $scope.next_filter = {};
            $scope.posts = [];
            $scope.loadPosts();
        };

        $scope.setGlobalFilter = function(filter) { $scope.global_filter = filter; };
        $scope.setScopeFilter = function(filter) { $scope.scope_filter = filter; };

        $scope.setPostStatus = function(post, status) {
            $http.post(api_url_post_edit, status, {params: {id: post.id}})
                .success(function (data) {
                    angular.extend(post, data['object']);
                })
                .error(function () {
                    /* warn user and retry till success*/
                });
            ;
        };

        $scope.gotoPost = function(post_id) {
            // close all elements befoe
            var content = $('#post_' + post_id);
            if (content.hasClass('in')) {
                content.removeClass('in');
            } else {
                var header = $('#header_' + post_id);
                var visible = $('#post_list .in');
                var scroll = $('#post_list');
                visible.removeClass('in');
                content.addClass('in');
                scroll.scrollTop(
                    scroll.scrollTop()+header.position().top
                );
            }
        };

        $scope.prevPost = function() {
            if ($scope.index > 0) {
                $scope.gotoPost($scope.posts[--$scope.index].id);
            }
        };

        $scope.nextPost = function() {
            if ($scope.index < $scope.posts.length-1) {
                $scope.gotoPost($scope.posts[++$scope.index].id);
            }
        };

        $scope.setIndex = function(index) { $scope.index = index; };

        $scope.keyPress = function($event) {
            var key = $event.keyCode;
            console.log(key);
            var post = $scope.posts[$scope.index];
            switch(key) {
                case 107: // k, prev
                    $scope.prevPost();
                    break;
                case 106: // j, next
                    $scope.nextPost();
                    break;
                case 109: // m, mark was read
                    $scope.setPostStatus(post, {was_read: !post.was_read});
                    break;
                case 115: // s, mark was starred
                    $scope.setPostStatus(post, {starred: !post.starred});
                    break;
            }
        };

        $scope.loadWebsites();
        // $scope.loadPosts();
    });
}());
