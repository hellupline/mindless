var app = angular.module('Mindless', ['ngRoute', 'ngSanitize', 'angular-blocks']);

app.config(['$interpolateProvider', '$locationProvider', '$httpProvider', '$routeProvider',
            function($interpolateProvider, $locationProvider, $httpProvider, $routeProvider) {
                $interpolateProvider.startSymbol('((');
                $interpolateProvider.endSymbol('))');
                // $locationProvider.html5Mode(true);
                $locationProvider.hashPrefix('!');
                $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
                $httpProvider.defaults.xsrfCookieName = 'csrftoken';
                $routeProvider
                    .when('/home', {
                        templateUrl: '/home.html'
                    })
                    .when('/brain/data/list/', {
                        templateUrl: '/brain/data_list.html',
                        controller: 'DataListController'
                    })
                    .when('/brain/data/add/', {
                        templateUrl: '/brain/data_add.html',
                        controller: 'DataAddController'
                    })
                    .when('/brain/data/detail/', {
                        templateUrl: '/brain/data_detail.html',
                        controller: 'DataDetailController'
                    })
                    .when('/brain/', {
                        redirectTo: '/brain/data/list/'
                    })
                    .when('/feed/post/list/', {
                        templateUrl: '/feed/post_list.html',
                        controller: 'PostListController'
                    })
                    .when('/feed/', {
                        redirectTo: '/feed/post/list/'
                    })
                    .when('/auth/user/login/', {
                        templateUrl: '/auth/user_login.html',
                        controller: 'UserLoginController'
                    })
                    .when('/auth/user/detail/', {
                        templateUrl: '/auth/user_detail.html',
                        controller: 'UserDetailController'
                    })
                    .when('/auth/user/add/', {
                        templateUrl: '/auth/user_add.html',
                        controller: 'UserAddController'
                    })
                    .when('/auth/', {
                        templateUrl: '/auth/user_login.html'
                    })
                    .otherwise({
                        redirectTo: '/home/'
                    });
            }]);

app.directive('whenScrolled', function() {
    return function(scope, elm, attr) {
        var raw = elm[0];

        elm.bind('scroll', function() {
            if (raw.scrollTop + raw.offsetHeight >= raw.scrollHeight - 250) {
                scope.$apply(attr.whenScrolled);
            }
        });
    };
});

function css_hates_me_and_dont_set_relative_height() {
    $('.fixed').height(
        $(window).height()-100
    );
}

$(window).resize(css_hates_me_and_dont_set_relative_height);
$(document).ready(css_hates_me_and_dont_set_relative_height);
