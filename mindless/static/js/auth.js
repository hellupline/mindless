(function() {
    'use strict';

    var api_url_login = '/auth/user/login/api/';
    var api_url_detail = '/auth/user/detail/api/';
    var api_url_add = '/auth/user/add/api/';

    app.controller('UserLoginController', function($scope, $http, $location) {
        $scope.breadcrumb_path = [
            {name: 'Auth', url:'/#!/auth/'},
            {name: 'User', url:'/#!/auth/user/'},
            {name: 'Login', url:'/#!/auth/user/login/'}
        ];

        $scope.userdata = {username: '', password: '', remember: true};

        $scope.Login = function() {
            $http.post(api_url_login, $scope.userdata);
        };
    });

    app.controller('UserDetailController', function($scope, $http, $location) {
        $scope.breadcrumb_path = [
            {name: 'Auth', url:'/#!/auth/'},
            {name: 'User', url:'/#!/auth/user/'},
            {name: 'Detail', url:'/#!/auth/user/detail/'}
        ];

        $scope.loadUser = function() {
            $http.get(api_url_detail)
                .success(function(data) {
                    $scope.userdata = data['object'];
                });
        };
    });

    app.controller('UserAddController', function($scope, $http, $location) {
        $scope.breadcrumb_path = [
            {name: 'Auth', url:'/#!/auth/'},
            {name: 'User', url:'/#!/auth/user/'},
            {name: 'Add', url:'/#!/auth/user/add/'}
        ];

        $scope.createUser = function() {
            $http.post(api_url_add, $scope.userdata);
        };
    });
}());
