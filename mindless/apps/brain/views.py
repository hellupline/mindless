import json

from flask import Blueprint, request
from flask.ext.login import login_required, current_user

from .models import db, Data, data_build_filter
from mindless.libs import views
from mindless.libs.session import get_or_fail


mod = Blueprint('brain', __name__, url_prefix='/brain')


@mod.route('/data/list/', methods=('GET',))
@login_required
@views.json
@views.db(db.session)
def data_list(session):
    limit, offset = views.get_slice(request.args)
    query = session.query(Data).filter(Data.owner == current_user)
    for filter in data_build_filter(request.args):
        query = query.filter(filter)
    query = query.offset(offset).limit(limit)
    return {
        'object_list': [data.map() for data in query.all()],
        'next_page': {'offset': offset+limit, 'limit': limit}
    }


@mod.route('/data/detail/', methods=('GET',))
@login_required
@views.json
@views.db(db.session)
def data_detail(session):
    data = get_or_fail(
        session, Data,
        Data.owner == current_user,
        Data.slug == request.args.get('slug')
    )
    return {'object': data.map()}


@mod.route('/data/add/', methods=('POST',))
@login_required
@views.json
@views.db(db.session)
def data_add(session):
    data = Data(owner=current_user)
    data.update(views.filter_dict(
        json.loads(request.data.decode('utf-8')),
        ('data', 'name', 'tags')
    ))

    session.add(data)
    session.commit()
    session.refresh(data)
    return {'object': data.map()}


@mod.route('/data/edit/', methods=('POST',))
@login_required
@views.json
@views.db(db.session)
def data_edit(session):
    data = get_or_fail(
        session, Data,
        Data.owner == current_user,
        Data.slug == request.args.get('slug')
    )
    data.update(views.filter_dict(
        json.loads(request.data.decode('utf-8')),
        ('data', 'name', 'tags')
    ))

    session.add(data)
    session.commit()
    session.refresh(data)
    return {'object': data.map()}


@mod.route('/data/delete/', methods=('POST',))
@login_required
@views.json
@views.db(db.session)
def data_delete(session):
    data = get_or_fail(
        session, Data,
        Data.owner == current_user,
        Data.slug == request.args.get('slug')
    )
    session.delete(data)
    return {'success': True}
