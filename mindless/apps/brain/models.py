# -*- coding: utf-8 -*-

from sqlalchemy import event
from sqlalchemy.sql import select

from mindless.extensions import db
from mindless.libs.models import ModelMixin
from mindless.libs.text import slugify, quote_tags, parse_tag_input
from mindless.apps.auth.models import User


class Data(db.Model, ModelMixin):
    __tablename__ = 'brain_data'
    tag_set = db.relationship(
        'Tag', secondary='brain_data_to_tag')
    owner = db.relationship('User')
    id = db.Column('id', db.Integer, primary_key=True)
    data = db.Column(db.Text)
    name = db.Column(db.String(255))
    slug = db.Column(db.String(255), unique=True)
    owner_id = db.Column(db.Integer, db.ForeignKey(User.id))
    _tags = []
    _tags_was_set = False

    def map(self):
        return dict(self, tags=self.tags, _tags=[t.slug for t in self.tag_set])

    @property
    def tags(self):
        if not self._tags_was_set:
            tags = [tag.slug for tag in self.tag_set]
        else:
            tags = self._tags
        return ' '.join(quote_tags(tags))

    @tags.setter
    def tags(self, value):
        if isinstance(value, list):
            value = ' '.join(quote_tags(value))
        self._tags = parse_tag_input(value)
        self._tags_was_set = True

    def update(self, values):
        data = dict(self)
        data.update(values)
        if '_tags' in data:
            data.pop('_tags')
        for key, value in data.items():
            if hasattr(self, key):
                setattr(self, key, value)


class Tag(db.Model, ModelMixin):
    __tablename__ = 'brain_tag'
    data_set = db.relationship(
        'Data', secondary='brain_data_to_tag', lazy='subquery')
    id = db.Column(db.Integer, primary_key=True)
    slug = db.Column(db.String(255), unique=True)


brain_data_to_tag_table = db.Table(
    'brain_data_to_tag',
    db.Column('data_id', db.Integer, db.ForeignKey(Data.id)),
    db.Column('tag_id', db.Integer, db.ForeignKey(Tag.id)),
    db.UniqueConstraint('data_id', 'tag_id', name='uix_1'))


def tag_set_slug(mapper, conn, tag):
    tag.slug = slugify(tag.slug)


def data_set_slug(mapper, conn, data):
    data.slug = slugify(data.name)


def data_set_tags(mapper, conn, data):
    def dict_row_map(result):
        return [dict(row) for row in result]

    def get_tags_from_db(tags, conn):
        result = conn.execute(select((Tag,)).where(Tag.slug.in_(tags)))
        tags = dict_row_map(result)
        result.close()
        return tags

    def create_tags(tags, conn):
        result = conn.execute(
            Tag.__table__.insert(),
            [{'slug': tag} for tag in tags]
        )
        result.close()

    if not data._tags:
        return None
    tags = data._tags

    # ensure we have all tags in db
    exists_tags = get_tags_from_db(tags, conn)
    created_slugs = [
        tag for tag in tags if tag not in
        [tag['slug'] for tag in exists_tags]
    ]
    if created_slugs:
        create_tags(created_slugs, conn)
        exists_tags.extend(get_tags_from_db(created_slugs, conn))

    # get ids in data.tag_set
    result = conn.execute(
        select((brain_data_to_tag_table.c.tag_id,))
        .where(brain_data_to_tag_table.c.data_id == data.id)
    )
    data_tag_set = [m2m['tag_id'] for m2m in result]
    result.close()

    # add only ids with are in exists_tags and not in data.tag_set
    m2m_add = [
        {'data_id': data.id, 'tag_id': tag['id']}
        for tag in exists_tags
        if tag['id'] not in data_tag_set
    ]
    if m2m_add:
        result = conn.execute(brain_data_to_tag_table.insert(), m2m_add)
        result.close()

    # remove ids with are in data.tag_set and not in in exists_tags
    m2m_remove = [
        tag_id for tag_id in data_tag_set
        if tag_id not in [tag['id'] for tag in exists_tags]
    ]
    if m2m_remove:
        result = conn.execute(
            brain_data_to_tag_table.delete()
            .where(brain_data_to_tag_table.c.data_id == data.id)
            .where(brain_data_to_tag_table.c.tag_id.in_(m2m_remove)))
        result.close()


def data_build_filter(params):
    if 'tags' in params:
        for tag in params.get('tags', '').split(' '):
            if tag:
                if tag[0] == '-':
                    filter = ~ Data.tag_set.any(Tag.slug == tag[1:])
                else:
                    filter = Data.tag_set.any(Tag.slug.like(tag+'%'))
                yield filter


event.listen(Data, 'before_insert', data_set_slug)
event.listen(Tag, 'before_insert', tag_set_slug)
event.listen(Data, 'after_insert', data_set_tags)
event.listen(Data, 'after_update', data_set_tags)
