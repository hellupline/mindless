# -*- coding: utf-8 -*-

import json

from flask import Blueprint, request, abort
from flask.ext.login import (
    login_user, logout_user, login_required, current_user)

from .models import db, User, create_user
from mindless.libs import views
from mindless.libs.session import get_or_fail
from mindless.libs.text import text2bool


mod = Blueprint('auth', __name__, url_prefix='/auth')


@mod.route('/user/login/', methods=('POST',))
@views.json
@views.db(db.session)
def auth_user_login(session):
    userdata = json.loads(request.data.decode('utf-8'))
    user = get_or_fail(
        session, User,
        User.username == userdata.get('username')
    )
    if user.check_password(userdata.get('password')):
        login_user(user, remember=text2bool(userdata.get('remember'), False))
        return user.map()
    abort(401)


@mod.route('/user/logout/', methods=('POST',))
@login_required
@views.json
def auth_user_logout():
    logout_user()
    return {'success': True}


@mod.route('/user/detail/', methods=('GET',))
@login_required
@views.json
def auth_user_detail():
    return {'object': current_user.map()}


@mod.route('/user/add/', methods=('POST',))
@views.json
@views.db(db.session)
def user_add(session):
    session.add(create_user(
        json.loads(request.data.decode('utf-8'))
    ))
    return {'success': True}
