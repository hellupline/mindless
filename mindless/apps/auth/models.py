# -*- coding: utf-8 -*-

import hashlib
import random

from flask.ext.login import UserMixin

from mindless.extensions import db, login_manager
from mindless.libs.models import ModelMixin


ascii_list = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
              'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
              'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
              'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
              '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '!', '"', '#',
              '$', '%', '&', "'", '(', ')', '*', '+', ',', '-', '.', '/', ':',
              ';', '<', '=', '>', '?', '@', '[', '\\', ']', '^', '_', '`', '{',
              '|', '}', '~', ' ', '"', ]


class User(db.Model, ModelMixin, UserMixin):
    __tablename__ = 'auth_user'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(255), unique=True, index=True)
    password = db.Column(db.String(255))
    email = db.Column(db.Text, unique=True, index=True)
    name = db.Column(db.Text)
    active = db.Column(db.Boolean)
    salt1 = db.Column(db.String(16))
    salt2 = db.Column(db.String(16))
    created_at = db.Column(db.DateTime)
    last_login = db.Column(db.DateTime)
    __table_args__ = (db.UniqueConstraint(
        'username', 'email',
        name='username_email_unique'),)

    _map_fields = ('username', 'email', 'name')

    def map(self):
        return dict(self)

    def check_password(self, password):
        return self.password == hash_password(
            self.salt1, self.salt2, password)


def hash_password(salt1, salt2, password):
    '''Using salt1 and salt2 hash the password'''
    return hashlib.sha512(
        '{salt1}{password}{salt2}'.format(**locals()).encode('utf-8')
    ).hexdigest()


def create_password(password):
    '''Creates random salt1 and salt2
    keeps a entropy by shuffling the ascii poll at each call'''
    random.shuffle(ascii_list)
    salt1 = ''.join(ascii_list[:16])
    salt2 = ''.join(ascii_list[-16:])
    return salt1, salt2, hash_password(salt1, salt2, password)


def create_user(userdata):
    '''Creates the User model with a hashed password
    from a non hashed password'''
    (userdata['salt1'],
     userdata['salt2'],
     userdata['password']) = create_password(userdata['password'])
    return User(**userdata)
