# -*- coding: utf-8 -*-

import json

from mindless.extensions import db
from mindless.apps.auth.models import create_user
from mindless.libs.tests import app, BaseTestCase
from .models import WebSite, Subscription, Post, Status


def fixture(session):
    users = [create_user({'name': 'Test User {}'.format(i),
                          'email': 'TestEmail{}@example.com'.format(i),
                          'username': 'username{}'.format(i),
                          'password': 'password'})
             for i in range(2)]
    websites = [WebSite(title='Test WebSite {}'.format(i),
                        url='w{}.example.com'.format(i),
                        rss_url='w{}.example.com'.format(i))
                for i in range(2)]
    subscriptions = [Subscription(user=users[i], website=websites[i],
                                  category='Test Subscription {}'.format(i))
                     for i in range(2)]
    posts = [Post(title='Test Post {}{}'.format(i, j),
                  url='w{}.example.com/{}'.format(i, j),
                  website=websites[i])
             for i in range(2) for j in range(4)]
    status = [Status(user=users[0], post=posts[0],
                     was_read=False, starred=False),
              Status(user=users[0], post=posts[1],
                     was_read=False, starred=True),
              Status(user=users[0], post=posts[2],
                     was_read=True, starred=False),
              Status(user=users[0], post=posts[3],
                     was_read=True, starred=True),
              Status(user=users[1], post=posts[4],
                     was_read=False, starred=False),
              Status(user=users[1], post=posts[5],
                     was_read=False, starred=True),
              Status(user=users[1], post=posts[6],
                     was_read=True, starred=False),
              Status(user=users[1], post=posts[7],
                     was_read=True, starred=True)]

    session.add_all(users + websites + subscriptions + posts + status)
    session.commit()


class TestViews(BaseTestCase):
    fixture = fixture

    def setUp(self):
        self.maxDiff = None
        target = {'name': 'Test User 0',
                  'email': 'TestEmail0@example.com',
                  'username': 'username0'}

        r = self.app.post('/auth/user/login/', data=json.dumps(
            {'username': 'username0', 'password': 'password'}
        ))

        data = json.loads(r.data.decode('utf-8'))
        self.assertEqual(data, target)

    def test_subscription_list(self):
        with app.test_request_context():
            target = {'object_list': [
                subscription.map() for subscription in (
                    db.session.query(Subscription)
                    .filter(Subscription.id == 1)
                )
            ]}

        r = self.app.get('/feed/subscription/list/')
        data = json.loads(r.data.decode('utf-8'))
        self.assertEqual(data, target)

    def test_post_list_no_filter(self):
        '''Tests post list with no filters'''
        with app.test_request_context():
            target = {'object_list': [
                post.map() for post in (
                    db.session.query(Post)
                    .order_by(-Post.id)
                    .filter(Post.id <= 4)
                )
            ]}

        r = self.app.get('/feed/post/list/')
        data = json.loads(r.data.decode('utf-8'))
        self.assertEqual(data, target)

    def test_post_list_filter_was_read(self):
        '''Tests post list with was_read filter'''
        with app.test_request_context():
            target = {'object_list': [
                post.map() for post in (
                    db.session.query(Post)
                    .order_by(-Post.id)
                    .filter(Post.id >= 3)
                    .filter(Post.id <= 4)
                )
            ]}

        r = self.app.get('/feed/post/list/?was_read=true')
        data = json.loads(r.data.decode('utf-8'))
        self.assertEqual(data, target)

    def test_post_list_filter_category(self):
        '''Tests post list with category filter'''
        with app.test_request_context():
            target = {'object_list': [
                post.map() for post in (
                    db.session.query(Post)
                    .order_by(-Post.id)
                    .filter(Post.id <= 4)
                )
            ]}

        r = self.app.get('/feed/post/list/?category=Test+Subscription+0')
        data = json.loads(r.data.decode('utf-8'))
        self.assertEqual(data, target)

    def test_post_list_filter_website(self):
        '''Tests post list with website filter'''
        with app.test_request_context():
            target = {'object_list': [
                post.map() for post in (
                    db.session.query(Post)
                    .order_by(-Post.id)
                    .filter(Post.id <= 4)
                )
            ]}

        r = self.app.get('/feed/post/list/?website=w0.example.com')
        data = json.loads(r.data.decode('utf-8'))
        self.assertEqual(data, target)
