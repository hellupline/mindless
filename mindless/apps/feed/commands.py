# -*- coding: utf-8 -*-

import os
import traceback
from concurrent.futures import ThreadPoolExecutor, as_completed

from .models import db, WebSite, parse_posts, pre_save_posts
from mindless import manager
from mindless.libs.logging import get_logger
from mindless.libs.network import download_to_temp


@manager.option('--slug', dest='slug', default=None, help='website slug')
def fetch_posts(slug=None):
    '''Fetch posts from websites'''

    logger = get_logger(
        '{}.fetch_posts'.format(__name__), 'logs/fetch_posts.log')

    def thread_(website):
        xml_file = None
        try:
            logger.info('download xml: {}'.format(website.slug))
            xml_file = download_to_temp(website.rss_url)
            if not xml_file:
                logger.info('fail to download: {}'.format(website.slug))
                return (), website
            logger.info('downloaded xml: {}'.format(website.slug))

            posts = list(parse_posts(xml_file))
            logger.info('parsed: {}: {}'.format(len(posts), website.slug))

            return posts, website
        except Exception:
            logger.error(traceback.format_exc())
        finally:
            if xml_file is not None:
                os.remove(xml_file)

    query = db.session.query(WebSite)
    if slug:
        query = query.filter(WebSite.slug == slug)

    logger.info('started fetch_posts')
    with ThreadPoolExecutor(max_workers=16) as executor:
        future_list = [
            executor.submit(thread_, website)
            for website in query
        ]
        fetch_posts = [
            future.result()
            for future in as_completed(future_list)
        ]
    logger.info('saving posts')
    for posts, website in fetch_posts:
        try:
            posts, status = pre_save_posts(posts, website)
            db.session.add_all(posts)
            db.session.add_all(status)
            db.session.commit()
        except Exception:
            db.session.rollback()
            logger.error(traceback.format_exc())

    logger.info('finished fetch_posts')
