# -*- coding: utf-8 -*-

from concurrent.futures import ThreadPoolExecutor, as_completed
from datetime import datetime

import feedparser
import requests
from io import BytesIO
from sqlalchemy import event
from dateutil import parser as date_parser
from lxml import etree

from mindless.extensions import db
from mindless.apps.auth.models import User
from mindless.libs.models import ModelMixin
from mindless.libs.text import slugify, text2bool


class WebSite(db.Model, ModelMixin):
    __tablename__ = 'feed_website'
    id = db.Column(db.Integer, primary_key=True)
    url = db.Column(db.Text)
    rss_url = db.Column(db.Text, unique=True, index=True)
    title = db.Column(db.String(255))
    subtitle = db.Column(db.String(255))
    slug = db.Column(db.String(255), unique=True)

    _map_fields = ('url', 'rss_url', 'title', 'subtitle', 'slug')

    def map(self):
        return dict(self)

    @property
    def post_count(self):
        return len(self.post_set)


class Post(db.Model, ModelMixin):
    __tablename__ = 'feed_post'
    id = db.Column(db.Integer, primary_key=True)
    author = db.Column(db.String(255))
    title = db.Column(db.String(255))
    summary = db.Column(db.Text)
    content = db.Column(db.Text)
    url = db.Column(db.Text, unique=True)
    created_at = db.Column(db.DateTime, default=datetime.now)

    website_rss_url = db.Column(db.ForeignKey(WebSite.rss_url))
    website = db.relationship(WebSite, backref='post_set')

    _map_fields = (
        'author', 'title', 'summary', 'content', 'url', 'created_at')

    def map(self):
        overwrite = {
            'created_at': self.created_at.isoformat(),
            'status': self.status_set[0].map()
        }
        return dict(self, **overwrite)


class Subscription(db.Model, ModelMixin):
    __tablename__ = 'feed_subscription'
    id = db.Column(db.Integer, primary_key=True)
    category = db.Column(db.String(255))

    website_rss_url = db.Column(db.ForeignKey(WebSite.rss_url))
    website = db.relationship(WebSite, backref='subscription_set')
    user_id = db.Column(db.ForeignKey(User.id))
    user = db.relationship(User, backref='subscription_set')
    __table_args__ = (db.UniqueConstraint(
        'website_rss_url', 'user_id',
        name='website_user_unique'),)

    _map_fields = ('category', 'website_rss_url')

    def map(self):
        return dict(self, website=self.website.map())


class Status(db.Model, ModelMixin):
    __tablename__ = 'feed_status'
    id = db.Column(db.Integer, primary_key=True)
    was_read = db.Column(db.Boolean, default=False)
    starred = db.Column(db.Boolean, default=False)

    post_id = db.Column(db.ForeignKey(Post.id))
    post = db.relationship(Post, backref='status_set')
    user_id = db.Column(db.ForeignKey(User.id))
    user = db.relationship(User, backref='status_set')
    __table_args__ = (db.UniqueConstraint(
        'post_id', 'user_id',
        name='post_user_unique'),)

    _map_fields = ('was_read', 'starred')

    def map(self):
        return dict(self)


def website_create(rss_url):
    orig_url = rss_url
    f = BytesIO(requests.get(rss_url).content)
    for el in etree.iterparse(f, tag=('link',), html=True):
        tag = el[1]
        if tag.attrib.get('type') == 'application/rss+xml':
            rss_url = tag.attrib.get('href')
            break

    if orig_url == rss_url:
        f.seek(0)
        feed = feedparser.parse(f.read())
    else:
        feed = feedparser.parse(rss_url)

    for link in feed['feed']['links']:
        if link['type'] == 'application/rss+xml':
            rss_url = link['href']

    feed2website_map = {
        'title': 'title',
        'subtitle': 'subtitle',
        'url': 'link',
    }
    website = {
        k1: feed['feed'].get(k2)
        for k1, k2 in feed2website_map.items()
    }
    website['rss_url'] = rss_url
    return website


def website_mass_create(websitedata_list):
    with ThreadPoolExecutor(max_workers=10) as executor:
        future_list = [
            executor.submit(website_create, websitedata)
            for websitedata in websitedata_list
        ]
        website_list = [
            future.result() for future in as_completed(future_list)
        ]
    return website_list


def website_set_slug(mapper, conn, website):
    website.slug = slugify(website.title)


def parse_posts(xml_file):
    post_map = {
        'author': 'author',
        'title': 'title',
        'summary': 'summary',
        'content': 'content',
        'url': 'link',
        'created_at': 'published',
    }
    xml = feedparser.parse(xml_file)
    for entry in xml['entries']:
        post = {k1: entry.get(k2) for k1, k2 in post_map.items()}
        if post['content']:
            post['content'] = post['content'][0]['value']
        if post['created_at']:
            post['created_at'] = date_parser.parse(post['created_at'])
        yield post


def pre_save_posts(posts, website):
    urls = [p['url'] for p in posts]
    if not urls:
        return (), ()
    urls_query = db.session.query(Post.url).filter(Post.url.in_(urls))
    existing_urls = [post.url for post in urls_query]

    posts_db = {}
    for post in posts:
        if post['url'] not in existing_urls and post['url'] not in posts_db:
            posts_db[post['url']] = Post(website=website, **post)
    posts = list(posts_db.values())

    users = db.session.query(User).join(Subscription).filter(
        Subscription.website == website)
    status = [Status(post=post, user=user) for post in posts for user in users]
    return posts, status


def post_build_filter(params):
    filters = {
        'was_read': lambda t: Status.was_read == text2bool(t),
        'starred': lambda t: Status.starred == text2bool(t),
        'category': lambda t: Subscription.category == t,
        'website': lambda t: WebSite.rss_url == t,
        'lt_id': lambda t: Post.id < t,
    }

    for key, value in filters.items():
        if key in params:
            yield value(params[key])


event.listen(WebSite, 'before_insert', website_set_slug)
