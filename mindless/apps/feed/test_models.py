# -*- coding: utf-8 -*-

from dateutil import parser as date_parser

from mindless.extensions import db
from mindless.libs.tests import app, BaseTestCase
from mindless.apps.auth.models import create_user
from .models import WebSite, Subscription, Status, parse_posts, pre_save_posts


XML_POST = (b'''<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0">
<channel>
 <title>RSS Title</title>
 <description>This is an example of an RSS feed</description>
 <link>http://www.example.com/main.html</link>
 <lastBuildDate>Mon, 06 Sep 2010 00:01:00 +0000 </lastBuildDate>
 <pubDate>Sun, 06 Sep 2009 16:20:00 +0000</pubDate>
 <ttl>1800</ttl>
 <item>
  <title>Test Title</title>
  <author>Test Author</author>
  <description>Test Description</description>
  <content>Test Content</content>
  <link>http://www.example.com/post/</link>
  <pubDate>Thu, 01 Jan 1970 00:00:00 +0000</pubDate>
 </item>
</channel>
</rss>''')


def fixture(session):
    users = [create_user({'name': 'Test User {}'.format(i),
                          'email': 'TestEmail{}@example.com'.format(i),
                          'username': 'username{}'.format(i),
                          'password': 'password'})
             for i in range(2)]
    websites = [WebSite(title='Test WebSite {}'.format(i),
                        url='w{}.example.com'.format(i),
                        rss_url='w{}.example.com'.format(i))
                for i in range(4)]
    subscriptions = [
        Subscription(
            user=users[i], website=websites[i+j],
            category='Test Subscription {}'.format(i+j)
        )
        for i in range(2) for j in range(3)
    ]

    session.add_all(users + websites + subscriptions)
    session.commit()


class TestViews(BaseTestCase):
    fixture = fixture

    def test_parse_posts(self):
        target = [{
            'author': 'Test Author',
            'content': 'Test Content',
            'created_at': date_parser.parse('Thu, 01 Jan 1970 00:00:00 +0000'),
            'summary': 'Test Description',
            'title': 'Test Title',
            'url': 'http://www.example.com/post/',
        }]
        self.assertEqual(target, list(parse_posts(XML_POST)))

    def test_pre_save_posts(self):
        target = [{'starred': False, 'was_read': False},
                  {'starred': False, 'was_read': False},
                  {'starred': False, 'was_read': False},
                  {'starred': False, 'was_read': False},
                  {'starred': False, 'was_read': False},
                  {'starred': False, 'was_read': False}]

        with app.test_request_context():
            for website in db.session.query(WebSite):
                posts = [{
                    'author': 'Test Author from {}'.format(website.slug),
                    'content': 'Test Content from {}'.format(website.slug),
                    'created_at': date_parser.parse(
                        'Thu, 01 Jan 1970 00:00:00 +0000'),
                    'summary': 'Test Description from {}'.format(website.slug),
                    'title': 'Test Title from {}'.format(website.slug),
                    'url': 'http://www.example.com/post/{}'.format(
                        website.slug),
                }]
                posts, status = pre_save_posts(posts, website)
                db.session.add_all(posts + status)
            db.session.commit()

            self.assertEqual(
                target, [s.map() for s in db.session.query(Status)])
