# -*- coding: utf-8 -*-

import json

from flask import Blueprint, request
from flask.ext.login import login_required, current_user
from sqlalchemy.orm.exc import NoResultFound

from .models import (
    db, WebSite, Post, Subscription, Status, post_build_filter, website_create)
from mindless.libs import views
from mindless.libs.session import get_or_fail


mod = Blueprint('feed', __name__, url_prefix='/feed')


@mod.route('/subscription/list/', methods=('GET',))
@login_required
@views.json
@views.db(db.session)
def subscription_list(session):
    query = session.query(Subscription).filter(
        Subscription.user == current_user)
    return {'object_list': [subscription.map() for subscription in query]}


@mod.route('/subscription/add/', methods=('POST',))
@login_required
@views.json
@views.db(db.session)
def subscription_add(session):
    data = json.loads(request.data.decode('utf-8'))
    url = data['url']
    try:
        website = session.query(WebSite).filter(
            WebSite.url == url or WebSite.rss_url == url
        ).one()
    except NoResultFound:
        website = WebSite(**website_create(url))
        session.add(website)
    subscription = Subscription(website=website, category=data['category'])
    session.add(subscription)
    return {'success': True}


@mod.route('/subscription/edit/', methods=('POST',))
@login_required
@views.json
@views.db
def subscription_edit(session):
    subscription = get_or_fail(
        session, Subscription,
        Subscription.user == current_user,
        WebSite.rss_url == request.args.get('url')
    )
    subscription.update(views.filter_dict(
        json.loads(request.data.decode('utf-8')),
        ('category')
    ))
    pass


@mod.route('/subscription/remove/')
@login_required
@views.json
@views.db(db.session)
def subscription_remove(session):
    subscription = get_or_fail(
        session, Subscription,
        Subscription.user == current_user,
        WebSite.rss_url == request.args.get('url')
    )
    session.delete(subscription)
    return {'success': True}


@mod.route('/post/list/', methods=('GET',))
@login_required
@views.json
@views.db(db.session)
def post_list(session):
    query = (
        session.query(Post).join(Status).join(WebSite).join(Subscription)
        .order_by(-Post.id).options(
            db.contains_eager('status_set'),
            db.contains_eager('website').contains_eager('subscription_set')
        )
        .filter(Status.user == current_user)
        .filter(Subscription.user == current_user)
    )

    for filter in post_build_filter(request.args):
        query = query.filter(filter)

    query = query.limit(20)
    return {'object_list': [post.map() for post in query]}
