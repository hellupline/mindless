# -*- coding: utf-8 -*-

from mindless.config import Default


SQLALCHEMY_DATABASE_URI = 'sqlite:///{}/dev.sqlite'.format(
    Default.PROJECT_ROOT)
DEBUG = TESTING = SQLALCHEMY_ECHO = True
