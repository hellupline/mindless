#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from flask.ext.script import Manager

from mindless import create_app
from mindless.config import Server as ServerConfig
from mindless.extensions import db
from flask.ext.migrate import MigrateCommand


app = create_app(config=ServerConfig)
manager = Manager(app)


@manager.command
def initdb():
    """Init/reset database."""

    db.drop_all()
    db.create_all()

manager.add_command('db', MigrateCommand)


# manager.add_option(
#     '-c', '--config', dest="config", required=False, help="config file")


if __name__ == '__main__':
    manager.run()
